package com.foodDelivery;
import java.util.*;

class FoodPanda{
    private static int counter = 0; // To keep track of the object references stored in ItemAddr
    private static Item[] ItemAddr = new Item[12]; //to store objects defined in functions of each case of switch case
    private static Scanner sc = new Scanner(System.in);
    private static int[] number = new int[12];
    private static int[] quantity = new int[12];
    public static void main(String[] args){
        out: while(true){
            System.out.println("---------------MENU---------------");
            System.out.println("Enter the category of choice");
            System.out.println("1. Soups\n2. Starters\n3. Main_Course\n4. Meal\n5. Exit & Proceed to Bill\n");
            System.out.println("-----------------------------------");
            String choice = sc.next();
            switch(choice){
                case "1":
                case "Soups":
                    soupMenu();
                    break;
                case "Starters":
                case "2":
                    starterMenu();
                    break;
                case "Main_Course":
                case "3":
                    mainCourseMenu();
                    break;
                case "Meal":
                case "4":
                    mealMenu();
                    break;
                case "Bill":
                case "5":
                    bill();
                    break out;
                default:
                    throw new IllegalStateException("Incorrect choice: " + choice);
            }
        }
    }
    private static void soupMenu() {
        System.out.println("\n\n*****Soups Menu*****");
        System.out.println("1. Tomato Soup\t\t\t" + "Rs 40");
        System.out.println("2. Sweet Corn Soup\t\t" + "Rs 58");
        System.out.println("3. Manchow Soup\t\t\t" + "Rs 60");
        System.out.println("0. Exit Soup Menu");
        System.out.println("*******************");
        System.out.println("\nHow many soups do you need?");
        int dishNoItems = sc.nextInt();
        if (dishNoItems == 0) {
            return;
        }
        Item[] ItemObj1 = new Item[dishNoItems];
        getDetails(dishNoItems, ItemObj1);
    }

    private static void getDetails(int dishNoItems, Item[] itemObj1) {
        System.out.println("Enter the Item number and its quantity");
        Dish dish = Dish.alooGobi;
        for (int i = 0; i < dishNoItems; i++) {
             number[i] = sc.nextInt();
             quantity[i] = sc.nextInt();
            itemObj1[i] = new Item(quantity[i], dish.getPrice(number[i]), dish.getName(number[i]));
            ItemAddr[counter++] = itemObj1[i];
        }
    }

    private static void starterMenu(){
        System.out.println("\n\nStarters Menu");
        System.out.println("4. Crispy Chilly Potato\t\t\t"+"Rs 101");
        System.out.println("5. Baby Corn Manchurian\t\t\t"+"Rs 106");
        System.out.println("6. Gobi Chilli\t\t\t\t"+"Rs 120");
        System.out.println("0. Exit Starter Menu");
        System.out.println("\nHow many starters do you need?");
        int dishNoItems = sc.nextInt();
        if (dishNoItems == 0) {
            return;
        }
        Item[] ItemObj2 = new Item[dishNoItems];
        getDetails(dishNoItems, ItemObj2);
    }

    private static void mainCourseMenu(){
        System.out.println("\n\nMain Courses Menu");
        System.out.println("7. Veg Schezwan\t\t\t"+"Rs 140");
        System.out.println("8. Hot Mushroom Garlic\t\t"+"Rs 140");
        System.out.println("9. Aloo Gobi\t\t\t"+"Rs 130");
        System.out.println("0. Exit Main Course Menu");
        System.out.println("\nHow many items from the main course menu do you need?");
        int dishNoItems = sc.nextInt();
        if (dishNoItems == 0) {
            return;
        }
        Item[] ItemObj3 = new Item[dishNoItems];
        getDetails(dishNoItems, ItemObj3);
    }

    private static void mealMenu(){
        System.out.println("\n\nMeal Menu");
        System.out.println("10. South Indian Meal\t\t\t"+"Rs 75");
        System.out.println("11. North Indian Meal\t\t\t"+"Rs 130");
        System.out.println("0. Exit Meal Menu");
        System.out.println("\nHow many Meal items do you need?");
        int dishNoItems = sc.nextInt();
        if (dishNoItems == 0) {
            return;
        }
        Item[] ItemObj4 = new Item[dishNoItems];
        getDetails(dishNoItems, ItemObj4);
    }
    private static void bill() {
        int i;
        System.out.println("Do You have a coupon code?(Y/N)----Today's OFFER : Rs 50 OFF");
        String answer = sc.next();
        outer : if(answer.equals("Y")) {
            System.out.print("Enter the code : ");
            String code = sc.next();
            String TodayCode = "FPANDA";
            if (code.equals(TodayCode)){
                try {
                    if (ItemAddr[0].CalcTotal() < 50) { //Total purchase is less than the coupon amount
                        throw new UnderFlowException("Coupon Amount Too Large!");
                    }
                }
                catch (UnderFlowException e)
                {
                    break outer;
                }
                System.out.println("-----------------------BILL----------------------");
                System.out.println("Item\t\t\tQuantity\t\t\tPrice");
                for (i = 0; i < counter; i++)
                    ItemAddr[i].PrintDetails();
                System.out.println("\n\nTOTAL BILL = " + ItemAddr[i - 1].CalcTotal());
                System.out.println("Coupon Offer = Rs. 50");
                System.out.println("FINAL PRICE : " + (ItemAddr[i - 1].CalcTotal() - 50));
                System.out.println("\n\n****THANK YOU*****");
                return;
            } else {
                System.out.println("INCORRECT CODE!\n\n");
            }
        }
        System.out.println("-----------------------BILL----------------------");
        System.out.println("Item\t\t\tQuantity\t\t\tPrice");
        for (i = 0; i < counter; i++)
            ItemAddr[i].PrintDetails();
        System.out.println("\n\nTOTAL BILL = " + ItemAddr[i - 1].CalcTotal());
        System.out.println("\n\n****THANK YOU*****");
    }
}
