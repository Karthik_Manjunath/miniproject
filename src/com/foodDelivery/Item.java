package com.foodDelivery;

class Item {
    private int quantity, rate;
    private static int total = 0;
    private String name;

    Item(int quantity, int rate, String name) {
        this.quantity = quantity;
        this.rate = rate;
        this.name = name;
        total += quantity * rate;
    }
    int CalcTotal(){
        return total;
    }
    void PrintDetails(){
        System.out.println(name+"\t\t"+quantity+"\t\t\t\t"+rate);
    }
}
