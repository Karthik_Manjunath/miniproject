package com.foodDelivery;
//This enumeration lists all dishes on the Menu
enum Dish{
    tomatoSoup(40, "Tomato Soup"), sweetCornSoup(58, "Sweet Corn Soup"), manchowSoup(60, "Manchow Soup"),
    crispyChilliPotato(101, "Crispy Chilli Potato"), babyCornManchurian(106, "Baby Corn Manchurian"), gobiChilli(120, "Gobi Chilli"),
    vegSchezwan(140, "Veg Schezwan"), hotMushroomGarlic(140, "Hot Mushroom Garlic"), alooGobi(130, "Aloo Gobi"),
    southIndianMeal(75, "South Indian Meal"), northIndianMeal(130, "North Indian Meal");
    private int price;
    private String name;
    Dish(int cost, String itemName){
        price = cost;
        name = itemName;
    }
    int getPrice(int number){
        return Dish.values()[number-1].price;
    }
    String getName(int number){
        return Dish.values()[number-1].name;
    }
}
